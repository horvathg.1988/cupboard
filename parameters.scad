use<extern/scad-utils/transformations.scad>
use<extern/scad-utils/linalg.scad>

fn=48;
epsilon=1;
SZEKRENY_W=1000;
SZEKRENY_H=880;
/* SZEKRENY_D=360; */
SZEKRENY_D=400;

LAP_W=18;
KORPUSZ_LAP_W=LAP_W;
POLC_LAP_W=LAP_W;

/* POLC_D=290; */
POLC_D=SZEKRENY_D-70;
POLC_TARTO_1=150;
POLC_TARTO_2=SZEKRENY_D-60;
POLC_H=180;
ELSO_POLC_H=POLC_H-LAP_W;
POLC_DB=floor((SZEKRENY_H-ELSO_POLC_H)/(POLC_H));
AJTO_FOGO_R=200;
AJTO_FOGO_D=30;
AJTO_ZARVA_SZOG=10;

CSAP_D=5;
CSAP_L=8;
CSAP_PEREM_D=7;
CSAP_PEREM_L=0.5;
CSAP_FURAT_H=CSAP_L+2;
CSAP_FURAT_D=CSAP_D;
CSAP_FORGO_Y=34;

CSAVAR_D=5;
CSAVAR_L=60;
TETO_CSAVAR_TAV_1=50;
TETO_CSAVAR_TAV_2=250;
TETO_NEGATIVE_GAP=20;

mx_forgo=translation([20,150,KORPUSZ_LAP_W-CSAP_L]);
mx_zarva=rotation([0,0,-90-5]);
vec_stop=[-120,KORPUSZ_LAP_W/2+CSAP_D/2,0];
elso_stop=take3(mx_forgo*(mx_zarva*vec4(vec_stop)));

// ------------------------
title_width = 290;
row_height = 15;

cols = [-1, 50, 114, 200, 215, 260];
rows = [0, -row_height, -row_height * 2, -row_height * 3, -row_height * 4];

// spacing tweaks to fit into the blocks
desc_x = 2; // column offset for start of small text
desc_y = -5; // row offset for start of small text
det_y = -12;  // row offset for start of detail text
desc_size = .75; // relative size of description text

DIM_HORZ=2;
DIM_VERT=2;

lines = [
    // horizontal lines
    [cols[0], rows[0], DIM_HORZ, title_width, 2],
    [cols[0], rows[1], DIM_HORZ, title_width, 1],
    [cols[2], rows[2], DIM_HORZ, title_width - cols[2] - 1, 1],
    [cols[3], rows[3], DIM_HORZ, title_width - cols[3] - 1, 1],
    [cols[0], rows[4] - 1, DIM_HORZ, title_width, 2],

    // vertical lines
    [0, 0, DIM_VERT, row_height * 4, 2],
    [cols[1], rows[0], DIM_VERT, row_height, 1],
    [cols[2], rows[0], DIM_VERT, row_height * 4, 1],
    [cols[3], rows[0], DIM_VERT, row_height * 4, 1],
    [cols[4], rows[3], DIM_VERT, row_height, 1],
    [cols[5], rows[3], DIM_VERT, row_height, 1],
    [title_width - 1, 0, DIM_VERT, row_height * 4, 2],
];

descs = [
  [cols[0] + desc_x, rows[0] + desc_y, DIM_HORZ, "Responsible dep", desc_size],
  [cols[1] + desc_x, rows[0] + desc_y, DIM_HORZ, "Technical reference", desc_size],
  [cols[2] + desc_x, rows[0] + desc_y, DIM_HORZ, "Creator", desc_size],
  [cols[3] + desc_x, rows[0] + desc_y, DIM_HORZ, "Approval person", desc_size],
  [cols[2] + desc_x, rows[1] + desc_y, DIM_HORZ, "Document type", desc_size],
  [cols[3] + desc_x, rows[1] + desc_y, DIM_HORZ, "Document status", desc_size],
  [cols[2] + desc_x, rows[2] + desc_y, DIM_HORZ, "Title", desc_size],
  [cols[3] + desc_x, rows[2] + desc_y, DIM_HORZ, "Identification number", desc_size],
  [cols[3] + desc_x, rows[3] + desc_y, DIM_HORZ, "Rev", desc_size],
  [cols[4] + desc_x, rows[3] + desc_y, DIM_HORZ, "Date of issue", desc_size],
  [cols[5] + desc_x, rows[3] + desc_y, DIM_HORZ, "Sheet", desc_size]
];


