include <parameters.scad>
module ajto(){
    lapw=KORPUSZ_LAP_W;
    h=POLC_H-2*2;
    ajtow=SZEKRENY_W-2*KORPUSZ_LAP_W-2*1;
    difference(){
	cube([ajtow,h,lapw]);
	translate([CSAP_L,CSAP_FORGO_Y,lapw/2]) rotate([0,-90,0]) cylinder(h=CSAP_FURAT_H+epsilon,d=CSAP_D, center=false, $fn=fn);
	translate([ajtow-CSAP_L,CSAP_FORGO_Y,lapw/2]) rotate([0,90,0]) cylinder(h=CSAP_FURAT_H+epsilon,d=CSAP_D, center=false, $fn=fn);
	translate([ajtow/2,h+AJTO_FOGO_R-AJTO_FOGO_D,0])
cylinder(h=LAP_W*2+epsilon, r=AJTO_FOGO_R, center=true, $fn=fn);
    }
}
projection(cut=true)
translate([0,0,-LAP_W/2])
ajto();
