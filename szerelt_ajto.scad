use <csap.scad>;
use <ajto.scad>;
include <parameters.scad>;
module szerelt_ajto(){
    lapw=LAP_W;
    szekrenyw=SZEKRENY_W;
    ajtow=szekrenyw-lapw*2-2;
    translate([0,-CSAP_FORGO_Y,-lapw/2]){
	ajto(lapw=lapw,ajtow=ajtow);
	translate([CSAP_L,CSAP_FORGO_Y,lapw/2]) rotate([0,-90,0]) csap();
	translate([ajtow-CSAP_L,CSAP_FORGO_Y,lapw/2]) rotate([0,90,0]) csap();
    }
}
rotate([90,0,0])
szerelt_ajto();
