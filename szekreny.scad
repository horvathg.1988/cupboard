use <oldal.scad>
use <szerelt_ajto.scad>
use <csap.scad>
use <polc.scad>
use <hatlap.scad>
use <teto.scad>
include <parameters.scad>;

union(){
// bal oldal
    rotate([90,0,90])
	fal();

// jobb oldal
    translate([SZEKRENY_W,0,0])
	rotate([0,0,-90])
	rotate([90,0,0])
	mirror([1,0,0])
	fal();

    for(i=[0:POLC_DB-1]){
// szerelt ajtok
	translate([KORPUSZ_LAP_W+0.5, 20, 150+i*POLC_H])
	    rotate([80,0,0])
	    szerelt_ajto();
// ajto megallito
	translate([KORPUSZ_LAP_W-CSAP_L, 45, 164+i*POLC_H])
	    rotate([0,90,0])
	    csap();
	// csap: ajto stop csukva
	// forgo pont
	/* translate([20,150+i*POLC_H,KORPUSZ_LAP_W-CSAP_L]) */
	/* 	rotate([0,0,-90-5]) */
	/* 	// relativ eltolas */
	/* 	translate([-120,KORPUSZ_LAP_W/2+CSAP_D/2,0]) */
	/* 	rotate([90,0,0]) */
	/* 	csap(); */

// polc tartó csapok
	translate([KORPUSZ_LAP_W-CSAP_L,POLC_TARTO_1,136+i*POLC_H])
	    rotate([0,90,0])
	    csap();
	translate([KORPUSZ_LAP_W-CSAP_L,POLC_TARTO_2,136+i*POLC_H])
	    rotate([0,90,0])
	    csap();

	translate([KORPUSZ_LAP_W,SZEKRENY_D-POLC_D,150-LAP_W/2+i*POLC_H])
	    polc();
    }

    translate([KORPUSZ_LAP_W,0,150-LAP_W/2+POLC_DB*POLC_H-TETO_NEGATIVE_GAP])
	teto();

    translate([LAP_W,SZEKRENY_D,0])
	hatlap();
}
