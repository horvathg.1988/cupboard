include<parameters.scad>
module teto(){
    difference(){
    cube([SZEKRENY_W-2*KORPUSZ_LAP_W,SZEKRENY_D,KORPUSZ_LAP_W]);

    translate([0,TETO_CSAVAR_TAV_1,KORPUSZ_LAP_W/2])
    rotate([0,90,0])
    cylinder(h=2*(CSAVAR_L-KORPUSZ_LAP_W), d=CSAVAR_D-1, center=true, $fn=fn);

    translate([0,TETO_CSAVAR_TAV_2,KORPUSZ_LAP_W/2])
    rotate([0,90,0])
    cylinder(h=2*(CSAVAR_L-KORPUSZ_LAP_W), d=CSAVAR_D-1, center=true, $fn=fn);

    translate([SZEKRENY_W-2*KORPUSZ_LAP_W,TETO_CSAVAR_TAV_1,KORPUSZ_LAP_W/2])
    rotate([0,90,0])
    cylinder(h=2*(CSAVAR_L-KORPUSZ_LAP_W), d=CSAVAR_D-1, center=true, $fn=fn);

    translate([SZEKRENY_W-2*KORPUSZ_LAP_W,TETO_CSAVAR_TAV_2,KORPUSZ_LAP_W/2])
    rotate([0,90,0])
    cylinder(h=2*(CSAVAR_L-KORPUSZ_LAP_W), d=CSAVAR_D-1, center=true, $fn=fn);
    }
    }
    
projection(cut=true)
translate([0,0,-LAP_W/2])    
teto();
