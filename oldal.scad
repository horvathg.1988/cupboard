use <csap.scad>
// include<dimlines.scad>
include <parameters.scad>
module fal(){
    FORGO_CSAP_D=CSAP_D;
    FORGO_CSAP_L=CSAP_FURAT_H;
    POLC_CSAP_L=CSAP_L;
    POLC_CSAP_D=CSAP_D;
    union(){
	difference(){
	    cube([SZEKRENY_D+LAP_W,SZEKRENY_H,KORPUSZ_LAP_W]);
	    for(i=[0:POLC_DB-1]){
		// ajto stop nyitva
		translate([45,164+i*POLC_H,KORPUSZ_LAP_W-POLC_CSAP_L])
		    cylinder(h=POLC_CSAP_L, d=POLC_CSAP_D, center=false, $fn=fn);
		// polc tarto
		translate([POLC_TARTO_1,136+i*POLC_H,KORPUSZ_LAP_W-POLC_CSAP_L])
		    cylinder(h=POLC_CSAP_L, d=POLC_CSAP_D, center=false, $fn=fn);
		translate([POLC_TARTO_2,136+i*POLC_H,KORPUSZ_LAP_W-POLC_CSAP_L])
		    cylinder(h=POLC_CSAP_L, d=POLC_CSAP_D, center=false, $fn=fn);
		// forgo
		translate([20,150+i*POLC_H,KORPUSZ_LAP_W-FORGO_CSAP_L])
		    cylinder(h=FORGO_CSAP_L, d=FORGO_CSAP_D, center=false, $fn=fn);
		// ajto stop csukva
		// forgo pont
		translate([20,150+i*POLC_H,KORPUSZ_LAP_W-POLC_CSAP_L])
		    rotate([0,0,-90-AJTO_ZARVA_SZOG])
		    // relativ eltolas
		    translate([-120,KORPUSZ_LAP_W/2+FORGO_CSAP_D/2,0])
		{
		    cylinder(h=POLC_CSAP_L, d=POLC_CSAP_D, center=false, $fn=fn);
		}
		// hátlap csavar
		translate([SZEKRENY_D+LAP_W/2,136+i*POLC_H,-1])
		cylinder(h=CSAVAR_L, d=CSAVAR_D, center=true, $fn=fn);
		// fedlap csavar
		translate([TETO_CSAVAR_TAV_1,SZEKRENY_H-LAP_W/2-TETO_NEGATIVE_GAP,0])
		cylinder(h=CSAVAR_L, d=CSAVAR_D, center=true, $fn=fn);
		translate([TETO_CSAVAR_TAV_2,SZEKRENY_H-LAP_W/2-TETO_NEGATIVE_GAP,0])
		cylinder(h=CSAVAR_L, d=CSAVAR_D, center=true, $fn=fn);
	    }
    		// hátlap csavar
		translate([SZEKRENY_D+LAP_W/2,136+4*POLC_H,0])
		cylinder(h=CSAVAR_L, d=CSAVAR_D, center=true, $fn=fn);
		// fedlap csavar

	}
	/* for(i=[0:POLC_DB-1]){ */
	/* } */

    }
}
projection(cut=true)
translate([0,0,-15])
fal();
