include <parameters.scad>
module csap (){
    furatd=CSAP_D;
    furatl=CSAP_L;
    peremd=CSAP_PEREM_D;
    pereml=CSAP_PEREM_L;
    letores=0.5;
    rotate_extrude(angle=360,convexity=10, $fn=fn)
    {
	polygon([
		    [0,0],
		    [furatd/2-letores,0],
		    [furatd/2,letores],
		    [furatd/2,furatl],
		    [peremd/2,furatl],
		    [peremd/2,furatl+pereml],
		    [furatd/2,furatl+pereml],
		    [furatd/2,2*furatl+pereml-letores],
		    [furatd/2-letores,2*furatl+pereml],
		    [0,2*furatl+pereml]
		    ]);
    }
};
csap();
