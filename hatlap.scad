include<parameters.scad>
module hatlap(){
    difference(){
	cube([SZEKRENY_W-2*LAP_W,KORPUSZ_LAP_W,SZEKRENY_H]);
	for(i=[0:POLC_DB]){
	    translate([0,KORPUSZ_LAP_W/2,136+i*POLC_H])
		rotate([0,90,0])
		cylinder(h=2*(CSAVAR_L-KORPUSZ_LAP_W), d=CSAVAR_D, center=true, $fn=fn);
	    translate([SZEKRENY_W-2*LAP_W,KORPUSZ_LAP_W/2,136+i*POLC_H])
		rotate([0,90,0])
		cylinder(h=2*(CSAVAR_L-KORPUSZ_LAP_W), d=CSAVAR_D, center=true, $fn=fn);
	}
    }
}
/*
*/
projection(cut=true)
translate([0,0,4])
rotate([0,90,0])
hatlap();
